# Screenshots 🎨

<p align="center">
<img src="https://gitlab.com/TakeoDanTonCu/dotfiles/-/raw/main/gitlab-img/desktop.png">
<img src="https://gitlab.com/TakeoDanTonCu/dotfiles/-/raw/main/gitlab-img/distro.png">
<img src="https://gitlab.com/TakeoDanTonCu/dotfiles/-/raw/main/gitlab-img/music.png">
<img src="https://gitlab.com/TakeoDanTonCu/dotfiles/-/raw/main/gitlab-img/rofi-app.png">
<img src="https://gitlab.com/TakeoDanTonCu/dotfiles/-/raw/main/gitlab-img/rofi-power.png">
</p>

# Setup 💻

- **OS:** [Arch Linux](https://archlinux.org/ "Arch Linux")
- **WM:** [i3](https://github.com/i3/i3 "i3")
- **Menus:** [Rofi](https://github.com/davatorium/rofi "Rofi")
- **Terminal:** [Kitty](https://github.com/kovidgoyal/kitty "Kitty")
- **Shell:** [Zsh](https://fr.wikipedia.org/wiki/Z_Shell "Zsh") + [Oh My Zsh](https://ohmyz.sh/ "Oh My Zsh") + [Powerlevel10k](https://github.com/romkatv/powerlevel10k "Powerlevel10k")
- **Compositor:** [Picom](https://github.com/yshui/picom "Picom")
- **Music Player:** [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp "ncmpcpp")
- **Editors:** [Vim](https://github.com/vim/vim "Vim") & [Doom Emacs](https://github.com/hlissner/doom-emacs "Doom Emacs")
- **Theme:**  [Catppuccin](https://github.com/catppuccin/catppuccin "Catppuccin")
